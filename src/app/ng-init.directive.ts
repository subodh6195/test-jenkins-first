import { Directive, Output, EventEmitter, OnInit, Input } from '@angular/core';

@Directive({
  selector: '[appNgInit]'
})
export class NgInitDirective implements OnInit {

  constructor() {
   }


   @Output('ngInit') initEvent: EventEmitter<any> = new EventEmitter();
 
   ngOnInit() {
       setTimeout(() => this.initEvent.emit(), 10);
   }

}
