export enum Direction {
    LEFT_RIGHT = 0,
    UP_DOWN = 1,
    RIGHT_LEFT = 2,
    DOWN_UP = 3,
}