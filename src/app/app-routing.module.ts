import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LufthansaHomeComponent } from './lufthansa-home/lufthansa-home.component';
import { TranslateService } from '@ngx-translate/core';


const routes: Routes = [
  // { path: '', redirectTo: '/home', pathMatch: 'full' },
  // { path: 'home',  component: LufthansaHomeComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {

  constructor(private translate: TranslateService) {
    translate.setDefaultLang('en');
}


}
