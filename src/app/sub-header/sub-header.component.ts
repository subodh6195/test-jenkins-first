import { Component, OnInit, EventEmitter, Output } from '@angular/core';

@Component({
  selector: 'app-sub-header',
  templateUrl: './sub-header.component.html',
  styleUrls: ['./sub-header.component.css']
})
export class SubHeaderComponent implements OnInit {


  @Output() nameSearched = new EventEmitter();
  name: string;
  constructor() { }
  
  ngOnInit() {
  }

  onSearchChange(value) {
    console.log(value);
    this.nameSearched.emit(value);
  }

}
