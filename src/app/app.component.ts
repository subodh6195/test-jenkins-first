import { Component, OnInit, ViewChild, ElementRef, AfterViewInit } from '@angular/core';
import * as d3 from 'd3';
import { Router } from '@angular/router';
import {
  AuthService,
  FacebookLoginProvider
} from 'angular-6-social-login';
import { DataService } from './data.service';
import { Direction } from './direction';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {

  constructor(public dataService: DataService, private socialAuthService: AuthService) {
  }
  DATA: any = [];
  envSvg: any;

  @ViewChild('envContainer', { static: true })
  envContainer: ElementRef;


  ngOnInit() {
    this.dataService.getJSON().subscribe(data => {
      this.DATA = data;
      console.log(this.DATA);
      setTimeout(() => {
        // tslint:disable-next-line: prefer-for-of
        for (let i = 0; i < this.DATA.length; i++) {
          this.envSvg = d3.select('#' + this.DATA[i].Key).append('svg').attr('width', '100%')
            .attr('height', '100%');
          this.updateEnvSvg(this.DATA[i].value);
        }
      }, 100);
    });
  }


  private updateEnvSvg(value): void {

    // tslint:disable-next-line: radix
    let width: number = Number.parseInt(String(this.envSvg.style('width')).replace('px', ''));
    let height = Number.parseInt(String(this.envSvg.style("height")).replace("px", ""));
    let minLength = width < height ? width : height;

    let margin = 5;
    let centerBoxSize = minLength / 3;
    let numOfBoxesAtOneSide = 4; // min 3 boxes should be there

    let surroundedBoxSize = centerBoxSize / (numOfBoxesAtOneSide - 2) - (numOfBoxesAtOneSide - 3) * margin / (numOfBoxesAtOneSide - 2);
    let centerBoxPosX = width / 2 - centerBoxSize / 2;
    let centerBoxPosY = height / 2 - centerBoxSize / 2;

    let direction = Direction.LEFT_RIGHT;
    let counter = 1;
    let layer = 1;
    let prevBoxCount = 0; // default is 0 as no layer is present
    let surroundedBoxTotalCount = (numOfBoxesAtOneSide - 1) * 4;
    let globalContainer = this.envSvg.selectAll("g")
      .data(value)
      .enter().append("g");

    let rect = globalContainer.append("rect")
      .attr('x', function (d: any, i: number) {
        if (i == 0) {
          d.isCenter = true;
          d.x = centerBoxPosX;
        }
        else {
          d.isCenter = false;
          //console.log("i : " + i + ", prevBoxCount : " + prevBoxCount + ", surroundedBoxTotalCount : " + surroundedBoxTotalCount)
          let layeredBoxIndex = i - prevBoxCount;
          let isInsideLayer = Math.floor((layeredBoxIndex / surroundedBoxTotalCount) + (layeredBoxIndex % surroundedBoxTotalCount == 0 ? 0 : 1)) != 1;
          if (isInsideLayer) {
            counter = 1;
            direction = Direction.LEFT_RIGHT
            layer++;
            numOfBoxesAtOneSide = numOfBoxesAtOneSide + 2;
            prevBoxCount = surroundedBoxTotalCount;
            surroundedBoxTotalCount = (numOfBoxesAtOneSide - 1) * 4;
            layeredBoxIndex = i - prevBoxCount;
          }

          //direction changes
          if (layeredBoxIndex > 1 && (layeredBoxIndex - 1) % (numOfBoxesAtOneSide - 1) == 0) {
            counter = 1;
            if (Direction.LEFT_RIGHT === direction)
              direction = Direction.UP_DOWN;
            else if (Direction.UP_DOWN === direction)
              direction = Direction.RIGHT_LEFT;
            else if (Direction.RIGHT_LEFT === direction)
              direction = Direction.DOWN_UP;
            else
              direction = Direction.LEFT_RIGHT;
          }

          // console.log("layer : " + layer + " | layeredBoxIndex : " + layeredBoxIndex + " | direction : " + direction + " | prevBoxCount : " + prevBoxCount);
          // console.log("counter : " + counter);

          if (direction == Direction.LEFT_RIGHT) {
            d.x = centerBoxPosX - (layer - 1) * (surroundedBoxSize + margin) + (counter - 2) * (surroundedBoxSize + margin);
            d.y = centerBoxPosY - (layer - 1) * (surroundedBoxSize + margin) - margin - surroundedBoxSize;
          }
          else if (direction == Direction.UP_DOWN) {
            d.x = centerBoxPosX + (layer - 1) * (surroundedBoxSize + margin) + centerBoxSize + margin;
            d.y = centerBoxPosY - (layer - 1) * (surroundedBoxSize + margin) + (counter - 2) * (surroundedBoxSize + margin);
          }
          else if (direction == Direction.RIGHT_LEFT) {
            d.x = centerBoxPosX + (layer - 1) * (surroundedBoxSize + margin) + centerBoxSize + (2 - counter) * margin - (counter - 1) * surroundedBoxSize;
            d.y = centerBoxPosY + (layer - 1) * (surroundedBoxSize + margin) + centerBoxSize + margin;
          }
          else if (direction == Direction.DOWN_UP) {
            d.x = centerBoxPosX - (layer - 1) * (surroundedBoxSize + margin) - surroundedBoxSize - margin;
            d.y = centerBoxPosY + (layer - 1) * (surroundedBoxSize + margin) + centerBoxSize + (2 - counter) * margin - (counter - 1) * surroundedBoxSize;
          }
          counter++;
        }
        return d.x;
      })
      .attr('y', function (d: any, i: number) {
        if (i == 0) {
          d.y = centerBoxPosY;
        }
        return d.y;
      })
      .attr('width', function (d: any, i: number) {
        if (i == 0)
          d.width = centerBoxSize;
        else
          d.width = surroundedBoxSize
        return d.width;
      })
      .attr('height', function (d: any, i: number) {
        if (i == 0)
          d.height = centerBoxSize;
        else
          d.height = surroundedBoxSize;
        return d.height;
      })
      .attr('fill', function (d, i) { return i == 0 ? 'red' : 'green' })
      .on('click', function (data, i) {
        let clickedNode = globalContainer.select(function (d) {
          if (data.env_name == d.env_name)
            return this;
        });
        let clickedData = clickedNode.data()[0];
        if (clickedData.isCenter)
          return;

        let targetNode = globalContainer.select(function (d) {
          if (d.isCenter)
            return this;
        });
        let targetData = targetNode.data()[0];

        let clickedRect = clickedNode.select('rect');
        let clickedText = clickedNode.select('text');
        let targetRect = targetNode.select('rect');
        let targetText = targetNode.select('text');

        let tempX = clickedData.x;
        let tempY = clickedData.y;
        let tempWidth = clickedData.width;
        let tempHeight = clickedData.height;

        clickedData.x = targetData.x;
        clickedData.y = targetData.y;
        clickedData.width = targetData.width;
        clickedData.height = targetData.height;
        clickedData.isCenter = true;

        targetData.x = tempX;
        targetData.y = tempY;
        targetData.width = tempWidth;
        targetData.height = tempHeight;
        targetData.isCenter = false;

        clickedRect.transition()
          .duration(1000)
          .attr('x', clickedData.x)
          .attr('y', clickedData.y)
          .attr('width', clickedData.width)
          .attr('height', clickedData.height);

        clickedText.transition()
          .duration(1000)
          .attr('x', function (d, i) { return d.x + d.width / 2; })
          .attr('y', function (d, i) { return d.y + d.height / 2; })
          .style('fill', 'white')
          .style('font-size', function (d) {
            if (d.isCenter)
              return "16px";
            return "8px";
          });

        targetRect.transition()
          .duration(1000)
          .attr('x', targetData.x)
          .attr('y', targetData.y)
          .attr('width', targetData.width)
          .attr('height', targetData.height);

        targetText.transition()
          .duration(1000)
          .attr('x', function (d, i) { return d.x + d.width / 2; })
          .attr('y', function (d, i) { return d.y + d.height / 2; })
          .style('fill', 'white')
          .style('font-size', function (d) {
            if (d.isCenter)
              return "16px";
            return "8px";
          });
      });

    let text = globalContainer.append("text")
      .attr('x', function (d, i) { return d.x + d.width / 2; })
      .attr('y', function (d, i) { return d.y + d.height / 2; })
      .style('fill', 'white')
      .style('font-size', function (d) {
        if (d.isCenter)
          return "16px";
        return "8px";
      })
      .text(function (d: any, i: number) {
        return d.env_name;
      })
      .attr("text-anchor", "middle");
  }




  public socialSignIn(socialPlatform: string) {
    let socialPlatformProvider;
    // tslint:disable-next-line: triple-equals
    if (socialPlatform == 'facebook') {
      socialPlatformProvider = FacebookLoginProvider.PROVIDER_ID;
    }
    this.socialAuthService.signIn(socialPlatformProvider).then(
      (userData) => {
        console.log(socialPlatform + ' sign in data : ', userData);
        // Now sign-in with userData
        // ...

      }
    );
  }


}





