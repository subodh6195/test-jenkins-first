import { DataService } from './data.service';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LufthansaHomeComponent } from './lufthansa-home/lufthansa-home.component';
import { ButtonsModule } from 'ngx-bootstrap/buttons';
import { AngularFontAwesomeModule } from 'angular-font-awesome';
import { SubHeaderComponent } from './sub-header/sub-header.component';

// import ngx-translate and the http loader
import { TranslateLoader, TranslateModule } from '@ngx-translate/core';
import { HttpClient, HttpClientModule } from '@angular/common/http';

import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { SearchPipe } from './search.pipe';
import { FormsModule } from '@angular/forms';
import { NgInitDirective } from './ng-init.directive';
import {
  SocialLoginModule,
  AuthServiceConfig,
  GoogleLoginProvider,
  FacebookLoginProvider,
} from "angular-6-social-login";

// Configs 
export function getAuthServiceConfigs() {
  const config = new AuthServiceConfig(
    [
      {
        id: FacebookLoginProvider.PROVIDER_ID,
        provider: new FacebookLoginProvider('733215800511777')
      }
    ]
  );
  return config;
}


@NgModule({
  declarations: [
    AppComponent,
    LufthansaHomeComponent,
    SubHeaderComponent,
    SearchPipe,
    NgInitDirective
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    SocialLoginModule,
    AngularFontAwesomeModule,
    ButtonsModule.forRoot(),
    HttpClientModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient]
      }
    })
  ],
  providers: [DataService,
    {
      provide: AuthServiceConfig,
      useFactory: getAuthServiceConfigs
    }],
  bootstrap: [AppComponent]
})
export class AppModule { }

// required for AOT compilation
export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http);
}
