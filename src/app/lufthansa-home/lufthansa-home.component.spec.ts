import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LufthansaHomeComponent } from './lufthansa-home.component';

describe('LufthansaHomeComponent', () => {
  let component: LufthansaHomeComponent;
  let fixture: ComponentFixture<LufthansaHomeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LufthansaHomeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LufthansaHomeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
