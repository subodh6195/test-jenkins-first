import { Component, OnInit } from '@angular/core';
import { DataService } from '../data.service';

@Component({
  selector: 'app-lufthansa-home',
  templateUrl: './lufthansa-home.component.html',
  styleUrls: ['./lufthansa-home.component.css']
})
export class LufthansaHomeComponent implements OnInit {

  constructor(public dataService: DataService) { }
  customerData = [];
  searchName: string;

  ngOnInit() {
    this.dataService.getJSON().subscribe(data => {
      this.customerData = data;
      console.log( this.customerData);
    });
  }


  // envData(data){
  //   return data.toString();
  // }


  nameSearched(event){
   this.searchName = event;
  }
}
